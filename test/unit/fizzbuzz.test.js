describe('fizzbuzz', () => {
const libs = require('../../src/fizzbuzz.js');
it ('should return Fizz if nimber is 1',() =>{
    const number = 1;
    const result = libs.fizzbuzz(number);
    expect(result).toBe(1);
  })
  it ('should return Fizz if nimber is 2',() =>{
    const number = 2;
    const result = libs.fizzbuzz(number);
    expect(result).toBe(2);
  })
  it ('should return Fizz if nimber is 3',() =>{
    const number = 3;
    const result = libs.fizzbuzz(number);
    expect(result).toBe("Fizz");
  })
  it ('should return Fizz if nimber is 4',() =>{
    const number = 4;
    const result = libs.fizzbuzz(number);
    expect(result).toBe(4);
  })
  it ('should return Buzz if nimber is 5',() =>{
    const number = 5;
    const result = libs.fizzbuzz(number);
    expect(result).toBe("Buzz");
  })
  it ('should return Fizz if nimber is 6',() =>{
    const number = 6;
    const result = libs.fizzbuzz(number);
    expect(result).toBe("Fizz");
  })
  it ('should return Fizz if nimber is 7',() =>{
    const number = 7;
    const result = libs.fizzbuzz(number);
    expect(result).toBe(7);
  })
  it ('should return Fizz if nimber is 8',() =>{
    const number = 8;
    const result = libs.fizzbuzz(number);
    expect(result).toBe(8);
  })
  it ('should return Fizz if nimber is 9',() =>{
    const number = 9;
    const result = libs.fizzbuzz(number);
    expect(result).toBe("Fizz");
  })
  it ('should return Buzz if nimber is 10',() =>{
    const number = 10;
    const result = libs.fizzbuzz(number);
    expect(result).toBe("Buzz");
  })
  it ('should return 11 if nimber is 11',() =>{
    const number = 11;
    const result = libs.fizzbuzz(number);
    expect(result).toBe(11);
  })
  it ('should return Fizz if nimber is 12',() =>{
    const number = 12;
    const result = libs.fizzbuzz(number);
    expect(result).toBe("Fizz");
  })
  it ('should return 13 if nimber is 13',() =>{
    const number = 13;
    const result = libs.fizzbuzz(number);
    expect(result).toBe(13);
  })
  it ('should return 14 if nimber is 14',() =>{
    const number = 14;
    const result = libs.fizzbuzz(number);
    expect(result).toBe(14);
  })
  it ('should return Fizz Buzz if nimber is 15',() =>{
    const number = 15;
    const result = libs.fizzbuzz(number);
    expect(result).toBe("FizzBuzz");
  })
  it ('should return Fizz Buzz if nimber is 30',() =>{
    const number = 30;
    const result = libs.fizzbuzz(number);
    expect(result).toBe("FizzBuzz");
  })
  it ('should return Buzz if nimber is 40',() =>{
    const number = 40;
    const result = libs.fizzbuzz(number);
    expect(result).toBe("Buzz");
  })
  it ('should return Fizz if nimber is 66',() =>{
    const number = 66;
    const result = libs.fizzbuzz(number);
    expect(result).toBe("Fizz");
  })
  it ('should return Fizz if nimber is 87',() =>{
    const number = 87;
    const result = libs.fizzbuzz(number);
    expect(result).toBe("Fizz");
  })
  it ('should return Buzz if nimber is 100',() =>{
    const number = 100;
    const result = libs.fizzbuzz(number);
    expect(result).toBe("Buzz");
  })
})